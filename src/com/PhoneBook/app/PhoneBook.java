package com.PhoneBook.app;

import java.io.IOException;

import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
import javax.microedition.pim.PIM;
import javax.microedition.pim.PIMException;
import com.sun.lwuit.Component;
import com.sun.lwuit.Display;
import com.sun.lwuit.Form;
import com.sun.lwuit.Image;
import com.sun.lwuit.animations.CommonTransitions;
import com.nokia.lwuit.components.FormItem;
import com.nokia.lwuit.components.FormItemListener;


public class PhoneBook extends MIDlet {
	
	private Form formPhoneBook;
	
	private FormItem callItem = null;
	private Image callImageIcon = null;
	
	private FormItem callLogItem = null;
	private Image callLogImageIcon = null;
	
	private FormItem contactItem = null;
	private Image contactImageIcon = null;
	
	private FormItem settingItem = null;
	private Image settingImageIcon = null;
	
	private FormItem aboutItem = null;
	private Image aboutImageIcon = null;
	
	public PhoneBook() {
		// TODO Auto-generated constructor stub
	}

	protected void destroyApp(boolean unconditional)
			throws MIDletStateChangeException {
		// TODO Auto-generated method stub

	}

	protected void pauseApp() {
		// TODO Auto-generated method stub

	}

	protected void startApp() throws MIDletStateChangeException {
		// TODO Auto-generated method stub
		Display.init(this);		
		formPhoneBook = new Form("Phone Book");
		
		try {
			callImageIcon = Image.createImage("/phone.png");
			callLogImageIcon = Image.createImage("/arrow_right.png");
			contactImageIcon = Image.createImage("/notebook.png");
			settingImageIcon = Image.createImage("/settings.png");
			aboutImageIcon = Image.createImage("/star.png");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		callItem = new FormItem("Call", callImageIcon);
		callLogItem = new FormItem("Call Log", callLogImageIcon);
		contactItem = new FormItem("Contact", contactImageIcon);
		
		contactItem.setFormItemListener(new FormItemListener() {
			
			public void notifyFormItemListener(FormItem arg0, Component arg1,
					boolean arg2) {
				// TODO Auto-generated method stub
				if(checkPermission("javax.microedition.pim.ContactList.read")==1){
					PhoneBookContact phbkContact = new PhoneBookContact(formPhoneBook, contactImageIcon);
					phbkContact.show();		
				}
				else{
					try {
						(PIM.getInstance()).openPIMList(PIM.CONTACT_LIST, PIM.READ_ONLY);
					} catch (PIMException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
									
			}
		});
		
		settingItem = new FormItem("Setting", settingImageIcon);
		settingItem.setFormItemListener(new FormItemListener() {
			
			public void notifyFormItemListener(FormItem arg0, Component arg1,
					boolean arg2) {
				// TODO Auto-generated method stub
				PhoneBookSettings pbSettings = new PhoneBookSettings(formPhoneBook, settingImageIcon);
				pbSettings.show();
			}
		});
		aboutItem = new FormItem("About", aboutImageIcon);
		
		formPhoneBook.addComponent(callItem);
		formPhoneBook.addComponent(callLogItem);
		formPhoneBook.addComponent(contactItem);
		formPhoneBook.addComponent(settingItem);
		formPhoneBook.addComponent(aboutItem);
		formPhoneBook.setTransitionInAnimator(CommonTransitions.createSlide(CommonTransitions.SLIDE_HORIZONTAL, true, 5));
		formPhoneBook.show();

	}

}
