package com.PhoneBook.app;


import java.util.Enumeration;
import javax.microedition.pim.*;
import com.nokia.lwuit.components.FormItem;
import com.sun.lwuit.Command;
import com.sun.lwuit.Form;
import com.sun.lwuit.Image;
import com.sun.lwuit.events.ActionEvent;
import com.sun.lwuit.events.ActionListener;

public class PhoneBookContact extends Form implements ActionListener{

	private Form backForm = null;
	private PIM pim = null;
	
	public PhoneBookContact(Form frm, Image imgIcon){
		this.backForm = frm;
		pim = PIM.getInstance();
		Command backCommand = new Command("Back",null,1);		
		this.setBackCommand(backCommand);
		this.addCommandListener(this);
		
		this.setTitle("Contact List");

		try{			
			String[] pimListNames = pim.listPIMLists(PIM.CONTACT_LIST);
			ContactList addrList = (ContactList)pim.openPIMList(PIM.CONTACT_LIST, PIM.READ_ONLY, pimListNames[0]);
			Enumeration enuContact = addrList.items();				
			while( enuContact.hasMoreElements()){					
				Contact _contactElement = (Contact)enuContact.nextElement();
				String[] strName = _contactElement.getStringArray(Contact.NAME, 0);
				String sFirstName = strName[Contact.NAME_GIVEN];
				String sLastName = strName[Contact.NAME_FAMILY];

				String strNumber  = _contactElement.getString(Contact.TEL, 0);
				if(!strName.equals(null) && !strNumber.equals(null) ){
					FormItem frmitm = new FormItem("Name : " + sFirstName + " " + sLastName,"Mobile :"+strNumber,imgIcon, false, FormItem.HIGHLIGHT_COLOR_BLUE);
					this.addComponent(frmitm);	
				}

			}				
        }
		catch( Exception e){
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		Command cmd = arg0.getCommand();
		if( cmd.getCommandName().equals("Back")){
			this.backForm.showBack();
		}
	}	
}
