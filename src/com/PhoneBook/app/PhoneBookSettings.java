package com.PhoneBook.app;

import com.nokia.lwuit.components.FormItem;
import com.sun.lwuit.Command;
import com.sun.lwuit.Form;
import com.sun.lwuit.Image;
import com.sun.lwuit.events.ActionEvent;
import com.sun.lwuit.events.ActionListener;

public class PhoneBookSettings extends Form implements ActionListener{

	private Form backForm = null;	
	
	public PhoneBookSettings(Form frm, Image img){
		this.backForm = frm;		
		Command backCommand = new Command("Back",null,1);		
		this.setBackCommand(backCommand);
		this.addCommandListener(this);		
		this.setTitle("Linked Accounts");
		
		try{
			FormItem frmitm = new FormItem("Facebook contacts","sync",img, false, FormItem.HIGHLIGHT_COLOR_BLUE);
			this.addComponent(frmitm);	
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		
	}
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		Command cmd = arg0.getCommand();
		if( cmd.getCommandName().equals("Back")){
			this.backForm.showBack();
		}
		
	}

}
